import initialState from './initialAppState';
import Types from '../../actions/actionTypes';

const isNotifying = (state = initialState.isNotifying, action) => {
	switch(action.type) {
		case Types.SHOW_NOTIFICATION:
			return true;
		case Types.HIDE_NOTIFICATION:
			return false;
		default:
			return state;
	}
};

export default isNotifying;
