import React, {Component} from 'react';
import LazyComponent from 'shared/components/LazyComponent';
import {injectAsyncReducer, removeAsyncReducer} from 'shared/configureStore';

class LazyReporting extends Component {

	componentWillUnmount() {
		removeAsyncReducer(this.props.store, 'Reporting');
	}

	render() {
		return (
			<LazyComponent
				{...this.props}
				getComponent={(callback) => {
					require.ensure([
						'../Reporting',
						'../../reducers/Reporting'
					], (require) => {
						injectAsyncReducer(this.props.store, 'Reporting', require('../../reducers/Reporting').default);
						callback(null, require('../Reporting'));
					},  'Finance-Reporting'); // Domain-Subdomain
				}}
			/>
		)
	}

}

export default LazyReporting;
