import React from 'react';
import LazyComponent from 'shared/components/LazyComponent';

const LazyFinance = (props) => (
	<LazyComponent
		{...props}
		getComponent={(callback) => {
			require.ensure([], require => {
				callback(null, require('../home/Home'));
			}, 'Finance'); // Domain-Subdomain
		}}
	/>
);

export default LazyFinance;
