import React, {Component} from 'react';
import LazyComponent from 'shared/components/LazyComponent';
import {injectAsyncReducer, removeAsyncReducer} from 'shared/configureStore';

class LazySRTS extends Component {

	componentWillUnmount() {
		removeAsyncReducer(this.props.store, 'SRTS');
	}

	render() {
		return (
			<LazyComponent
				{...this.props}
				getComponent={(callback) => {
					require.ensure([
						'../SRTS',
						'../../reducers/SRTS'
					], (require) => {
						injectAsyncReducer(this.props.store, 'SRTS', require('../../reducers/SRTS').default);
						callback(null, require('../SRTS'));
					},  'Finance-SRTS'); // Domain-Subdomain
				}}
			/>
		);
	}
}

export default LazySRTS;
