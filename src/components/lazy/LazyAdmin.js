import React from 'react';
import LazyComponent from 'shared/components/LazyComponent';

const LazyAdmin = (props) => (
	<LazyComponent
		{...props}
		getComponent={(callback) => {
			require.ensure([], require => {
				callback(null, require('../Admin'));
			}, 'Finance-Admin'); // Domain-Subdomain
		}}
	/>
);

export default LazyAdmin;
