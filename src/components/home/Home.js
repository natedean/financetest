import PropTypes from 'prop-types';
import React from 'react';
import Paper from 'material-ui/Paper';
import HomeMenu from 'shared/components/HomeMenu/HomeMenu';

/**
 * @function Home
 * @description
 * Entry page to the app
 * @return {object} - jsx
 */
const Home = ({routes}) => {
	return (
		<Paper style={{ padding: '1rem', backgroundColor: 'tan' }}>
			<div className="domainHome">
				<HomeMenu routes={routes}/>
			</div>
		</Paper>
	);
};

Home.propTypes = {
	rowBackgroundColor: PropTypes.string
};

export default Home;
