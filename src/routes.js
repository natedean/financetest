import React from 'react';

import {createMenu} from 'shared/services/menuHelpers';

import LazyFinance from './components/lazy/LazyFinance';
import LazySRTS from './components/lazy/LazySRTS';
import LazyReporting from './components/lazy/LazyReporting';
import LazyAdmin from './components/lazy/LazyAdmin';

// not injected with store, not configured with proper top level routes menu
const links = [
	{path:'/finance', name:'Finance', subRoutes: [
		{path:'/srts', name:'SRTS'},
		{path:'/reporting', name:'Reporting'},
		{path:'/admin', name:'Admin'}
	]}
];

// I know it's not ideal to repeat the same data structure here, but it's literally the only way I could get it to work
// Believe me... I tried fancier stuff...
// At some point, I've got to be pragmatic and do it the "dumb way" if it works
export const createRoutes = (store) => {
	const financeRoutes = createMenu(links)[0].subRoutes;

	return [
		{path:'/finance', name:'Finance', component: () => <LazyFinance store={store} routes={financeRoutes}/>,
			subRoutes: [
				{path:'/srts', name:'SRTS', component: () => <LazySRTS store={store}/>},
				{path:'/reporting', name:'Reporting', component: () => <LazyReporting store={store}/>},
				{path:'/admin', name:'Admin', component: () => <LazyAdmin store={store}/>}
			]
		}
	];
};

